Objectives:
- Have an application that connects to O365 via GraphAPI
- Application uses a USER ACCOUNT set up for DELEGATION
- ... in order to SEND email AS a SHARED MAILBOX
- ... using Public Client Flow / Resource Owner Password Credential Flow
(this is a deprecated approach, preferred approach is to use Application authorization + Policy limits)


1) Application

- Application registered in Azure Active Directory
- Authentication
	- Platform Web is added
		- Redirect URL = http://localhost/
	- Advanced Settings
		- Allow Public Client Flows -> Yes
- Tenant Id, Client Id are relevant - secret will NOT be used

- API Permissions - All DELEGATION permissions, with ADMIN Consent
	Mail.Read.Shared
	Mail.ReadWrite.Shared
	Mail.Send.Shared
	User.Read
	

2) Shared Mailbox
- A shared mailbox has been created

3) User for application / delegation
- User "x@y.com" is created - this is the user who will access the mailbox
- User has rights to Shared Mailbox - can read & send


4) Application code 
- List folders in shared mailbox
- List Mails with filters (Completed, Read)
- Flag emails as processed (Completed, Read)
- Send mail from Shared Mailbox - with attachments of various sizes




4) Test user has given consent for the application using a special url.
This asks the application user to log in interactivelly, and give consent. 
Alternatively, admin consent for all the permissions could be given.

For user consent:
- Log into azure portal using username / password of the service account
- Edit the URL below and add Tenant and Client IDs, then copy-paste into browser.
- Ignore the response with token - if the response comes, the grant has been given.



More info / examples here: https://ashiqf.com/2021/03/16/call-microsoft-graph-api-as-a-signed-in-user-with-delegated-permission-in-power-automate-or-azure-logic-apps-using-http-connector/


https://login.microsoftonline.com/<tenant_id>/oauth2/v2.0/authorize?client_id=<client_id>
&response_type=code
&redirect_uri=http%3A%2F%2Flocalhost%2F
&response_mode=query
&scope=offline_access
%20https://graph.microsoft.com/Mail.Read.Shared
%20https://graph.microsoft.com/Mail.ReadWrite.Shared
%20https://graph.microsoft.com/Mail.Send.Shared
%20https://graph.microsoft.com/User.Read
Objectives:
- Have an application that connects to O365 via GraphAPI
- Application uses a USER ACCOUNT set up for DELEGATION
- ... in order to SEND email AS a SHARED MAILBOX
- ... using Public Client Flow / Resource Owner Password Credential Flow
(this is a deprecated approach, preferred approach is to use Application authorization + Policy limits)


1) Application

- Application registered in Azure Active Directory
- Authentication
	- Platform Web is added
		- Redirect URL = http://localhost
	- Advanced Settings
		- Allow Public Client Flows -> Yes
- Tenant Id, Client Id, Client Secret are relevant

- API Permissions - All DELEGATION permissions
	Mail.Read.Shared
	Mail.ReadWrite.Shared
	Mail.Send.Shared
	User.Read
	offline_access

- Additional permission (to facilitate login - authetnication for TestHarness does not work without this):
https://management.core.windows.net
corresponds to Azure Service Management / user_impersonation

- Admin Consent to all API permissions	

2) Shared Mailbox
- A shared mailbox has been created

3) User for application / delegation
- User "x@y.com" is created - this is the user who will access the mailbox
- User has rights to Shared Mailbox - can read & send


4) Application code 
- List folders in shared mailbox
- List Mails with filters (Completed, Read)
- Flag emails as processed (Completed, Read)
- Send mail from Shared Mailbox - with attachments of various sizes



Explicit consent (if no admin consent)$
https://login.microsoftonline.com/<tenant_id>/oauth2/v2.0/authorize?client_id=<client_id>
&response_type=code
&redirect_uri=http%3A%2F%2Flocalhost%2F
&response_mode=query
&scope=offline_access
%20https://graph.microsoft.com/Mail.Read.Shared
%20https://graph.microsoft.com/Mail.ReadWrite.Shared
%20https://graph.microsoft.com/Mail.Send.Shared
%20https://graph.microsoft.com/User.Read
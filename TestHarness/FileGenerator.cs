﻿using GraphClient.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestHarness
{
    /// <summary>
    /// Utility Class - Generates empty files / attachments
    /// (all filled with letter 'A')
    /// </summary>
    internal class FileGenerator
    {
        /// <summary>
        /// Generates a list of dummy files of varying lengths
        /// First 3 files: Length 200.000
        /// Further files: length 5.500.000
        /// </summary>
        /// <param name="number">Number of files, default 3</param>
        /// <returns>List of generated files for testing</returns
        public static List<EmailAttachment> GetFiles(int number = 3)
        {
            var list = new List<EmailAttachment>();
            for(int i = 0; i < number; i++)
            {
                var a = new EmailAttachment()
                {
                    Name = "File-" + i.ToString() +".txt"
                    ,
                    Extension = ".txt"
                    ,
                    Length = (i < 3) ? 200000 : 3500000
                    
                };
                var ms = new MemoryStream();
                var sw = new StreamWriter(ms);
                {
                    for(int b = 0; b < a.Length; b++)
                    {
                        sw.Write('A');
                    }
                }


                /*
                //Testing - write out the file into local FS
                using (var fs = new FileStream(a.Name, FileMode.Create))
                {
                    ms.Position = 0;
                    ms.CopyTo(fs);
                    System.Diagnostics.Debug.WriteLine(fs.Name);
                }
                */

                a.Bytes = ms;
                ms.Position = 0;
                list.Add(a);
            }
            return list;

            
        }
    }
}

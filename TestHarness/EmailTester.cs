﻿using GraphClient;
using GraphClient.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Graph;

namespace TestHarness
{
    /// <summary>
    /// Helper class for testing email sending capability 
    /// </summary>
    internal class EmailTester
    {

        /// <summary>
        /// Sends an email with 4 attachments of varying sizes
        /// </summary>
        /// <param name="client"></param>
        /// <param name="sharedMailboxUser"></param>
        /// <param name="attachmentCount"></param>
        public static void SendEmail(Client client, string sharedMailboxUser, string senderEmail, string senderName, string recipient, int attachmentCount = 4)
        {
            //var recipient = "ziga.jakhel@gmail.com";
            var subject = "TEST subject " + Guid.NewGuid().ToString();
            var body = "TEST BODY - " + subject;
            
            var msg = new Message()
            {
                //Define both fields for sender - From is the "on behalf of" shared mailbox, sender is the actual sender 
                //This shows as "<senderEmail> on behalf of <sharedMailboxUser>"
                //The From needs to be set correctly for this to work
                //The Sender o365 / Exchange will set automatically - one can also omit the Sender and message will still send correctly
                From = new Recipient() { EmailAddress = new EmailAddress() { Address = sharedMailboxUser, Name = senderName } },
                Sender = new Recipient() { EmailAddress = new EmailAddress() { Address = senderEmail, Name = senderName } }

            };
            
            //Create a new list of To-Recipients
            var recipients = new List<Recipient>();
            recipients.Add(new Recipient() { EmailAddress = new EmailAddress() { Address = recipient } });
            msg.ToRecipients = recipients;

            //Could also do the same for CC-Recipients
            
            //
            msg.Subject = subject;
            msg.Body = new ItemBody()
            {
                ContentType = BodyType.Text
                ,
                Content = body
            };

            //Generate attachments (dummy files)
            // <= 3 - direct attachments
            // >3 large files with upload session
            var files = FileGenerator.GetFiles(attachmentCount);

            var mailClient = new EmailClient();
            var task = mailClient.SendSharedEmail(client, sharedMailboxUser, msg, files);
            task.Wait();
            var status = task.Status;
   


        }

        /// <summary>
        /// Downloads all attachments and store them as files
        /// </summary>
        /// <param name="client"></param>
        /// <param name="sharedMailboxUser"></param>
        /// <param name="messages"></param>
        public static void DownloadAttachments(Client client, string sharedMailboxUser, List<Message> messages)
        {
            var mailClient = new EmailClient();
            messages.ForEach(m =>
            {
                if (m.HasAttachments.HasValue && m.HasAttachments.Value)
                {
                    //This returns fully populated list of attachments incl. bytes
                    var attTask = mailClient.GetAttachments(client, sharedMailboxUser, m.Id);
                    attTask.Wait(120000);
                    var atts = attTask.Result;

                    foreach (var a in atts)
                    {
                        string fileName = m.Id + " - " + a.Name;
                        if (a is FileAttachment)
                        {
                            var fa = a as FileAttachment;

                            using (var fs = new FileStream(fileName, FileMode.Create))
                            {
                                fs.Write(fa.ContentBytes);
                                fs.Flush();
                            }
                            Console.WriteLine("..." + fileName);
                        }
                        else Console.WriteLine("..." + fileName + "... NOT a file");
                    }
                }

            });
        }

        

    }
}

﻿using Azure.Identity;
using GraphClient;
using GraphClient.Email;
using Microsoft.Graph;
using Microsoft.Identity.Client;

namespace TestHarness
{


    internal class Program
    {
         static ClientConfiguration config;

        static void Main(string[] args)
        {

            //Read the configuration file - passed in as command line argument
            var filePath = args[0];
            config = ClientConfiguration.Deserialize(filePath);
            var sharedMailboxUser = config.SharedMailbox;

            var senderEmail = args[1];
            var senderName = args[2];
            var recipient = args[3];
            int noFiles = (args.Length == 5) ? int.Parse(args[4]) : 4;

            Azure.Core.AccessToken oboToken;
            string stringToken = String.Empty;
            GraphClient.Client client = null;

            if (config.AuthenticationFlow == enAuthenticationFlow.OnBehalfOf
                || config.AuthenticationFlow == enAuthenticationFlow.AuthenticationToken)
            {

                var interactiveOptions = new InteractiveBrowserCredentialOptions
                {
                    TenantId = config.TenantId,
                    ClientId = config.ClientId,
                    AuthorityHost = AzureAuthorityHosts.AzurePublicCloud,
                    // MUST be http://localhost or http://localhost:PORT
                    // See https://github.com/AzureAD/microsoft-authentication-library-for-dotnet/wiki/System-Browser-on-.Net-Core
                    RedirectUri = new Uri("http://localhost"),
                };

                //Authenticate in browser - a new window is opened
                // https://docs.microsoft.com/dotnet/api/azure.identity.interactivebrowsercredential
                var creds = new InteractiveBrowserCredential(interactiveOptions);
                var res = creds.Authenticate();

                //Getting the JWT Token
                Azure.Core.TokenRequestContext trc = new Azure.Core.TokenRequestContext(config.Scopes.ToArray(), null, null, config.TenantId);
                oboToken = creds.GetToken(trc);
                stringToken = oboToken.Token;
                var len = stringToken.Length;

                //Normally the token would be stored in a secure cache & a refresh mechanism implemented
                //For the test harness we are just using the token direct to create the client

                //This attaches the JWT Token to any GraphClient requests
                //For production use, a proper cache should be used vs. passing a simple string
                client = GraphClient.Client.Create(config, stringToken, "");

            }
            else if (config.AuthenticationFlow == enAuthenticationFlow.AuthorizationCode)
            {
                //This has not been properly tested yet, your mileage may vary
                Console.WriteLine("Enter Authorization Code:");
                var authCode = Console.ReadLine();
                Console.WriteLine("... OK. Creating client.");
                client = GraphClient.Client.Create(config, "", authCode);
            }
            else
            {
                //For other authentication flows, no interaction is needed - all read from config
                client = GraphClient.Client.Create(config);
            }

            //Get User Profile
            var upcTask = UserProfileClient.GetUserProfile(client);
            upcTask.Wait(20000);
            var mail = upcTask.Result.Mail;
            var name = upcTask.Result.DisplayName;
            Console.WriteLine(string.Format("User: {0} / Email: {1}", name, mail));
            
            
            //Send an email via the shared mailbox
            Console.WriteLine("Sending Email...");
            Thread.Sleep(0);
            EmailTester.SendEmail(client, sharedMailboxUser, mail, name, recipient, noFiles);
            Console.WriteLine("\t... mail sent.");


            //List Mail folders in Shared Mailbox
            Console.WriteLine("Getting Shared Mailbox folders...");
            var mailClient = new EmailClient();
            var task = mailClient.ListSharedMailFolders(client, sharedMailboxUser);
            task.Wait(10000);
            task.Result.ToList().ForEach(f => Console.WriteLine(f.DisplayName));
            Console.WriteLine("\t... folders retrieved.");


            //List Mails in Shared Mailbox - Inbox Folder
            Console.WriteLine();
            Console.WriteLine("Getting Shared Inbox messages...");
            var cTask = mailClient.ListSharedInboxMessages(client, sharedMailboxUser, true);
            cTask.Wait(10000);
            var messages = cTask.Result.ToList<Message>();
            messages.ForEach(m => Console.WriteLine(String.Format("{0} : {1} : {2} : {3}", m.Sender.EmailAddress.Address, m.ReceivedDateTime, m.Subject, m.ParentFolderId)));
            Console.WriteLine("\t... messages retrieved.");


            //Download Files - if any attachments present
            Console.WriteLine();
            Console.WriteLine("Downloading attachments...");
            EmailTester.DownloadAttachments(client, sharedMailboxUser, messages);
            Console.WriteLine("\t... attachments downloaded.");

            //Mark Mails as processed
            Console.WriteLine();
            Console.WriteLine("Marking messages as processed...");
            mailClient.MarkMessagesAsProcessedAndUpdate(client, sharedMailboxUser, messages);
            Console.WriteLine("\t... messages marked.");

            Console.WriteLine();
            Console.WriteLine("All done!");
        }
    }
}
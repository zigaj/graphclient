﻿using Microsoft.Graph;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GraphClient.Email
{
    /// <summary>
    /// Email attachment DTO - enables late loading of files if necessary
    /// </summary>
    public class EmailAttachment
    {
        public string Name { get; set; }
        public System.IO.Stream Bytes { get; set; }
        public string Extension { get; set; }
        public long Length { get; set; }

    }


    public class EmailClient
    {
        int MINIMUM_SIZE_FOR_UPLOAD_SESSION = 3145728;
        //int MINIMUM_SIZE_FOR_UPLOAD_SESSION = 2000000; //This works  and eats less memory

        /// <summary>
        /// Lists logged-on users's mail folders
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<IUserMailFoldersCollectionPage> ListMailFolders(Client client)
        {
            var response = await client.Instance.Me.MailFolders.Request().GetAsync();
            return response;

        }

        /// <summary>
        /// Lists logged-on user's messages from a given mail folder
        /// </summary>
        /// <param name="client"></param>
        /// <param name="mailFolderId"></param>
        /// <returns></returns>
        public async Task<IMailFolderMessagesCollectionPage> ListMessages(Client client, string mailFolderId)
        {
            var messages = await client.Instance.Me
                .MailFolders[mailFolderId]
                .Messages
                .Request()

                //.Select("id, sender,subject, body, from, torecipients, ccrecipients, receiveddatetime") // You can filter properties to get - useful for reducing data
                .GetAsync();

            return messages;
        }

        /// <summary>
        /// Returns a message, without Attachments
        /// </summary>
        /// <param name="client"></param>
        /// <param name="sharedMailboxUser"></param>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public async Task<Message>  GetMessage(Client client, string sharedMailboxUser, string messageId)
        {
            var request =
            client.Instance
            .Users[sharedMailboxUser]
            .MailFolders.Inbox
            .Messages[messageId]
            //.Select("sender,subject, from, receiveddatetime")
            .Request();
            
            var msg = await request.GetAsync();
            return msg;

        }

        public async Task<IMessageAttachmentsCollectionPage> GetAttachments(Client client, string sharedMailboxUser, string messageId)
        {
            var request =
            client.Instance
            .Users[sharedMailboxUser]
            .MailFolders.Inbox
            .Messages[messageId]
            .Attachments
            .Request();

            var msg = await request.GetAsync();
            return msg;

        }

        public async Task<Microsoft.Graph.Attachment> GetAttachment(Client client, string sharedMailboxUser, string messageId, string attachmentId)
        {
            var request =
            client.Instance
            .Users[sharedMailboxUser]
            .MailFolders.Inbox
            .Messages[messageId]
            .Attachments[attachmentId]
            //.Select("sender,subject, from, receiveddatetime")
            .Request();

            var att = await request.GetAsync();
            return att;
        }



        /// <summary>
        /// Lists the Mailbox folders of a given user (shared mailbox)
        /// </summary>
        /// <param name="client"></param>
        /// <param name="sharedMailboxUser">Email address of shared mailbox</param>
        /// <returns></returns>
        public async Task<IUserMailFoldersCollectionPage> ListSharedMailFolders(Client client, string sharedMailboxUser)
        {
            var response = await client.Instance.Users[sharedMailboxUser].MailFolders.Request().GetAsync();
            return response;

        }

       /// <summary>
       /// Lists The User's Inbox messages - filtered as required
       /// </summary>
       /// <param name="client"></param>
       /// <param name="sharedMailboxUser">Email address of shared mailbox</param>
       /// <param name="unprocessedOnly">True/False - if true, only unread & not-flagged-complete mails are retrieved</param>
       /// <returns></returns>
        public async Task<IMailFolderMessagesCollectionPage> ListSharedInboxMessages(Client client, string sharedMailboxUser, bool unprocessedOnly = false)
        {

            var request =
            client.Instance
            .Users[sharedMailboxUser]
            .MailFolders.Inbox
            .Messages
            //.Select("sender,subject, from, receiveddatetime")
            .Request()
            .Select("id, sender,subject, from, torecipients, ccrecipients, receiveddatetime, isread, flag, hasattachments, attachments");
            if (unprocessedOnly) request.Filter("not (IsRead eq true and Flag/FlagStatus eq 'complete')");

            var messages = await request.GetAsync();
            return messages;
        }


        /// <summary>
        /// Marks a list of messages as processed and updates flags on server
        /// </summary>
        /// <param name="client"></param>
        /// <param name="sharedMailboxUser"></param>
        /// <param name="messages"></param>
        public void MarkMessagesAsProcessedAndUpdate(Client client, string sharedMailboxUser, IEnumerable<Message> messages)
        {
            var taskList = new List<Task>();
            var list = new List<Message>(messages);
            for(int i = 0; i < list.Count; i++)
            {
                var m = list[i];

                var newMsg = new Message();
                newMsg.Id = m.Id;
                newMsg.IsRead = true;
                newMsg.Flag = new FollowupFlag() { FlagStatus = FollowupFlagStatus.Complete };
                
                var tsk = client.Instance
                .Users[sharedMailboxUser]
                .MailFolders.Inbox
                .Messages[m.Id]
                .Request()
                .Select("IsRead, Flag/FlagStatus")
                .UpdateAsync(newMsg);

                taskList.Add(tsk);
                

                System.Diagnostics.Debug.WriteLine("Msg " + m.Id + " updated");
            }
            Task.WaitAll(taskList.ToArray(), 120000);
            

        }


        /// <summary>
        /// Send email from a given user's mailbox (any user)
        /// Processes attachments (using upload sessions for large files if necessary)
        /// </summary>
        /// <param name="client"></param>
        /// <param name="sharedMailboxUser">User's email - in our case Shared Mailbox</param>
        /// <param name="newMessage">Message to be sent</param>
        /// <param name="attachments">List of attachments (as we are working with Stream objects this is separate, not yet converted to byte arrays)</param>
        /// <returns></returns>
        public async Task SendSharedEmail(Client client, string sharedMailboxUser, Message newMessage, List<EmailAttachment> attachments = null)
        {
            

            Message draft = await client.Instance
                        .Users[sharedMailboxUser]
                        .MailFolders
                        .Drafts //Create DRAFT
                        .Messages
                        .Request()
                        //.WithMaxRetry(5)
                        .AddAsync(newMessage);

            if (attachments != null)
            {
                //Add attachments
                for (int i = 0; i < attachments.Count; i++)
                {
                    {
                        var a = attachments[i];

                        if (a.Length < MINIMUM_SIZE_FOR_UPLOAD_SESSION) // 3 Mb default, reduced
                        {
                            string mimeType = MimeTypes.GetMimeType(a.Extension);
                            var fileAttachment = new FileAttachment()
                            {

                                ODataType = "#microsoft.graph.fileAttachment",
                                ContentType = mimeType,
                                ContentId = a.Name,
                                Name = a.Name
                            };
                            fileAttachment.ContentBytes = new byte[(int)a.Length];
                            a.Bytes.Write(fileAttachment.ContentBytes, 0, (int)a.Bytes.Length);
                           
                            try
                            {
                                await client.Instance
                                       .Users[sharedMailboxUser]
                                       .Messages[draft.Id]
                                       .Attachments
                                       .Request()
                                       .AddAsync(fileAttachment);
                            }

                            catch (System.Exception ex)
                            {
                                System.Diagnostics.Debug.WriteLine(ex);
                                Console.WriteLine(ex.Message);
                            }

                        }
                        else
                        {
                            // Attachments >= 3 Mb

                            var attachmentItem = new AttachmentItem
                            {
                                AttachmentType = AttachmentType.File,
                                Name = a.Name,
                                Size = a.Bytes.Length
                            };

                            var uploadSession = await client.Instance
                                    .Users[sharedMailboxUser]
                                    .Messages[draft.Id]
                                    .Attachments
                                    .CreateUploadSession(attachmentItem)
                                    .Request()
                                    .PostAsync();

                            var maxSliceSize = 320 * 1024; // 320 KB - Change this to your slice size. 5MB is the default.
                            a.Bytes.Position = 0;

                            var largeFileUploadTask = new LargeFileUploadTask<FileAttachment>(uploadSession, a.Bytes, maxSliceSize);

                            // upload away with relevant callback
                            IProgress<long> progressCallback = new Progress<long>(prog => { });
                            try
                            {
                                var uploadResult = await largeFileUploadTask.UploadAsync(progressCallback);
                                if (!uploadResult.UploadSucceeded)
                                {
                                    // error
                                    System.Diagnostics.Debug.WriteLine(string.Format("Error - Mail {0} - Attachment {1}", newMessage.Subject, a.Name));
                                }
                            }
                            catch (ServiceException e)
                            {
                                // exception
                                System.Diagnostics.Debug.WriteLine(e.Message);
                                System.Diagnostics.Debug.WriteLine(e.StackTrace);
                                throw e;
                            }

                        }

                    }
                }

               

            }
            //Send email

            await client.Instance
               .Users[sharedMailboxUser]
               .Messages[draft.Id]
               .Send()
               .Request()
               .WithMaxRetry(5)
               .PostAsync();

            

            

        }
    }
}

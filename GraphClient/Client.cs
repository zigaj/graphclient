﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Azure.Core;
using Azure.Identity;
using IdentityModel.Client;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using Microsoft.Rest.Azure.Authentication;

namespace GraphClient
{
    public class Client
    {
      

        /// <summary>
        /// Client Configuration as used when creating the instance
        /// </summary>
        public ClientConfiguration Configuration { get; private set; }

        /// <summary>
        /// Actual GraphServiceClient instance
        /// </summary>
        public GraphServiceClient Instance { get; private set; }

        /// <summary>
        /// Client Factory Method
        /// </summary>
        /// <param name="clientConfiguration"></param>
        /// <returns></returns>
        public static Client Create(ClientConfiguration clientConfiguration, string AuthenticationToken= "", string authorizationCode = "")
        {
            var instance = new Client();
            instance.Configuration = clientConfiguration;
            instance.Instance= GetClient(clientConfiguration, AuthenticationToken) ;
            return instance;
         
        }


        private static GraphServiceClient GetClient(ClientConfiguration clientConfiguration, string authenticationToken = "", string authorizationCode = "")
        {
            var options = new TokenCredentialOptions { AuthorityHost = AzureAuthorityHosts.AzurePublicCloud };

            TokenCredential credential;
            
            //Http Handler to handle proxy configuration
            IHttpProvider httpProvider = null;
            var httpClientHandler = new HttpClientHandler() { AllowAutoRedirect = false };
            if (clientConfiguration.Proxy_Authentication)
            {
                httpClientHandler.UseProxy = true;
                httpClientHandler.Proxy = new System.Net.WebProxy(clientConfiguration.Proxy_Url)
                {
                    Credentials = new System.Net.NetworkCredential(clientConfiguration.Proxy_Username, clientConfiguration.Proxy_Password)
                };
                httpProvider = new HttpProvider(httpClientHandler, true);
            }

            //Get the Graph Service Client
            switch (clientConfiguration.AuthenticationFlow)
            {
                //Application authentication - uses application permissions
                case enAuthenticationFlow.Application:
                    {
                        credential = new ClientSecretCredential(clientConfiguration.TenantId, clientConfiguration.ClientId, clientConfiguration.ClientSecret, options);
                        var client = new GraphServiceClient(credential, clientConfiguration.Scopes, httpProvider);
                        return client;
                    }
                //Resource Owner Password Credentials Flow
                //Can use delegated permissions, requires "Public Flows" to be enables
                case enAuthenticationFlow.ROPC:
                    {
                        credential = new UsernamePasswordCredential(clientConfiguration.Username, clientConfiguration.Password, clientConfiguration.TenantId, clientConfiguration.ClientId, options); ;
                        var client = new GraphServiceClient(credential, clientConfiguration.Scopes, httpProvider);
                        return client;
                    }
                //On-Behalf-Of flow for middle-tier services (not tested)
                case enAuthenticationFlow.OnBehalfOf:
                    {
                        //https://docs.microsoft.com/en-us/graph/sdks/choose-authentication-providers?tabs=CS#on-behalf-of-provider
                        //https://docs.microsoft.com/en-us/graph/sdks/choose-authentication-providers?tabs=Java#on-behalf-of-provider
                        var cca = ConfidentialClientApplicationBuilder
                        .Create(clientConfiguration.ClientId)
                        .WithTenantId(clientConfiguration.TenantId)
                        .WithClientSecret(clientConfiguration.ClientSecret)
                        .Build();

                        var authProvider = new DelegateAuthenticationProvider(async (request) =>
                        {
                            var assertion = new UserAssertion(authenticationToken);
                            var result = await cca.AcquireTokenOnBehalfOf(clientConfiguration.Scopes, assertion).ExecuteAsync();
                            var accessToken = result.AccessToken;
                            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
                        });
                        var client = new GraphServiceClient(authProvider, httpProvider);
                        return client;
                    }
                //Authorization code flow
                //Takes the authorization code returned as a result to the login url
                case enAuthenticationFlow.AuthorizationCode:
                {
                    
                    // https://docs.microsoft.com/dotnet/api/azure.identity.authorizationcodecredential
                    var authCodeCredential = new AuthorizationCodeCredential(clientConfiguration.TenantId,
                        clientConfiguration.ClientId, clientConfiguration.ClientSecret, authorizationCode);

                        var client= new GraphServiceClient(authCodeCredential, clientConfiguration.Scopes, httpProvider);
                        return client;
                }
                //Standard flow for a user logged into the application using a JWT
                //Injects the application JWT into the client request headers on demand
                //Better implementation would pass a reference to a token factory / cache
                case enAuthenticationFlow.AuthenticationToken:
                {

                        var authProvider = new DelegateAuthenticationProvider(async (request) =>
                        {
                            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authenticationToken);
                        });

                        var client = new GraphServiceClient(authProvider, httpProvider);
                        return client;
                    }

                default:
                    return null;
            }
        }
            
           
        


    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace GraphClient
{
    [Serializable]
    public enum enAuthenticationFlow
    {
        Application,
        ROPC,
        OnBehalfOf,
        AuthorizationCode,
        AuthenticationToken
    }
    /// <summary>
    /// Client Configuration DTO - mirrors the configuration file
    /// </summary>
    public class ClientConfiguration
    {
        
        public enAuthenticationFlow AuthenticationFlow { get; set; }
       
        public string ClientId { get; set; }

        public string TenantId { get; set; }

        public string ClientSecret { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string SharedMailbox { get; set; }

        public IEnumerable<string> Scopes { get; set; }

        public bool Proxy_Authentication { get; set; }
        public string Proxy_Url { get; set; }
        public string Proxy_Username { get; set; }
        public string Proxy_Password { get; set; }


        public static void Serialize(ClientConfiguration configuration, string fileName)
        {
            
            var options = new JsonSerializerOptions { WriteIndented = true };
            using (var fs = new FileStream(fileName, System.IO.FileMode.Create))
            using (var sw = new StreamWriter(fs))
            {
                sw.Write(JsonSerializer.Serialize<ClientConfiguration>(configuration, options));
                sw.Flush();
            }
        
        }

        public static ClientConfiguration Deserialize(string fileName)
        {
            using (var fs = new FileStream(fileName, System.IO.FileMode.Open))
            using (var sr = new StreamReader(fs))
            {
                var options = new JsonSerializerOptions() { ReadCommentHandling = JsonCommentHandling.Skip };
                var config = JsonSerializer.Deserialize<ClientConfiguration>(sr.ReadToEnd(), options);
                var res = Validate(config);
                if (res.Count == 0) return config;
                else throw new AggregateException(res);
            }
        }

        public ClientConfiguration()
        {
            AuthenticationFlow = enAuthenticationFlow.OnBehalfOf;
            Proxy_Authentication = false;
        }

        private static List<Exception> Validate(ClientConfiguration config)
        {
            var res = true;
            var exceptions = new List<Exception>();

            res &= ValidateString("ClientId", config.ClientId, exceptions);
            res &= ValidateString("TenantId", config.TenantId, exceptions);
            res &= ValidateString("SharedMailbox", config.SharedMailbox, exceptions);

            if (config.AuthenticationFlow == enAuthenticationFlow.Application)
            {
              
                var scopesOk = (config.Scopes.Count() == 1) && (config.Scopes.First() == "https://graph.microsoft.com/.default");
                if (!scopesOk)
                {
                    res &= false;
                    exceptions.Add(new Exception("Scopes - for Application Mode Scope needs to contain 1 element only - 'https://graph.microsoft.com/.default'"));
                }
            }
            else if ( config.AuthenticationFlow == enAuthenticationFlow.OnBehalfOf 
                || config.AuthenticationFlow == enAuthenticationFlow.AuthenticationToken 
                || config.AuthenticationFlow == enAuthenticationFlow.AuthorizationCode)
            {
                res &= ValidateString("ClientSecret", config.ClientSecret, exceptions);
            }
            else
            {
                res &= ValidateString("Username", config.Username, exceptions);
                res &= ValidateString("Password", config.Password, exceptions);
            }
            
            if (config.Proxy_Authentication)
            {
                res &= ValidateString("Proxy_Url", config.Proxy_Url, exceptions);
                res &= ValidateString("Proxy_Username", config.Proxy_Username, exceptions);
                res &= ValidateString("Proxy_Password", config.Proxy_Password, exceptions);
            }

            return exceptions;
        }

    private static bool ValidateString(string name, string value, List<Exception> exceptions)
    {
        if (String.IsNullOrEmpty(value))
        {
            exceptions.Add(new Exception(string.Format("{0} is empty", name)));
            return false;
        }
        else return true;
    }


    }

}

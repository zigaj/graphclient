﻿using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphClient
{
    public class UserProfileClient
    {

        /// <summary>
        /// Returns the logged-on user's profile
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public static async Task<User> GetUserProfile(Client client)
        {
            var user = await client.Instance.Me.Request().GetAsync();
            return user;
        }
    }
}

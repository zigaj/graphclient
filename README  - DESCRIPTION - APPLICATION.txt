Objectives:
- Have an application that connects to O365 via GraphAPI
- Application uses the application identity
- ... in order to SEND email AS a SHARED MAILBOX
- ... securing access to only the one mailbox using APPLICATION ACCES POLICY

1) Application

- Application registered in Azure Active Directory
- Authentication
	- Platform Web is added
		- Redirect URL = http://localhost/
- Tenant Id, Client Id, Client Secret are relevant - write them down

- API Permissions - All APPLICATION permissions with ADMIN CONSENTT
	Mail.Read.Shared
	Mail.ReadWrite.Shared
	Mail.Send.Shared

	

2) Shared Mailbox
- A shared mailbox has been created

3) Limit application access to shared mailbox only
- Create a Mail-Enabled Security Group (using O365 Admin console)
- Add the Shared Mailbox to the Mail-Enabled Security Group
- Use Powershell to create a new Application Access Policy for the Security Group and Application
-> https://docs.microsoft.com/en-us/graph/auth-limit-mailbox-access


4) Application code
- Add a single scope only: "https://graph.microsoft.com/.default"
- List folders in shared mailbox
- List Mails with filters (Completed, Read)
- Flag emails as processed (Completed, Read)
- Send mail from Shared Mailbox - with attachments of various sizes